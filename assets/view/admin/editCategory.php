<!doctype html>
<?php 
    require_once '../../controllers/CategoriaController.php';
    
    if(isset($_GET['id'])){
        
    $db = new Mysql();
    $categorias= new CategoriaController($db);
    
        if(isset($_GET['acao'])&&$_GET['acao']=='update'){ 
            $categoriaEditar = $categorias->acaoEditarCategoria($_GET['id'],$_POST['nome']);
        }else{
            $categoriaEditar = $categorias->acaoEncontrarCategoria($_GET['id']);
        }          
    }else{
        /* redirecionar para algum lugar */
    }
?>
<html>
<head>
  <title>Webjump | Backend Test | Add Category</title>
  <meta charset="utf-8">

<link  rel="stylesheet" type="text/css"  media="all" href="../../css/style.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
<meta name="viewport" content="width=device-width,minimum-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script></head>
<!-- Header -->
<?php include 'header.php'; ?>

  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Editar Categoria</h1>
    <span style="color:green;font-weight:bold;"> <?php if(isset($_GET['sucesso']) && $_GET['sucesso']==1){ echo "Editado com sucesso!"; } ?></span>
    <form action="editCategory.php?acao=update&id=<?php echo $categoriaEditar['idCateg']; ?>" method="post" accept-charset="UTF-8">
      <div class="input-field">
        <label for="category-name" class="label">Categoria Nome *</label>
        <input type="text" id="category-name" class="input-text" name="nome" value="<?php echo $categoriaEditar['nomeCateg']; ?>" required />
        
      </div>
      <div class="input-field">
        <label for="category-code" class="label">Código</label>
        <input type="text" id="category-code" class="input-text" value="<?php echo $categoriaEditar['codigoCateg']; ?>" readonly />
        
      </div>
      <div class="actions-form">
        <a href="categories.php" class="action back">Voltar</a>
        <input class="btn-submit btn-action"  type="submit" value="Salvar" />
      </div>
    </form>
  </main>
  <!-- Main Content -->

<!-- Footer -->
<?php include 'footer.php'; ?>

</body>
</html>
