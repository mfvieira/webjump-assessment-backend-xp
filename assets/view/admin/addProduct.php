<!doctype html>
<?php 
    require_once '../../controllers/ProdutoController.php';
    require_once '../../controllers/CategoriaController.php';
    
    $db = new Mysql();
    $categorias = new CategoriaController($db);
    $listaCategorias = $categorias->acaoListarTodasCategorias();
    
    if(isset($_GET['acao'])&& $_GET['acao']='create'){
        
    $db = new Mysql();
    $produtos = new ProdutoController($db);
    $criado = $produtos->acaoCriarProduto($_POST['nome'],$_POST['sku'],$_POST['descricao'],$_POST['quantidade'],$_POST['preco'],$_POST['categorias']);
    }
?>
<html>
<head>
  <title>Webjump | Backend Test | Add Product</title>
  <meta charset="utf-8">

<link  rel="stylesheet" type="text/css"  media="all" href="../../css/style.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
<meta name="viewport" content="width=device-width,minimum-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>

<!-- jQuery  -->
<script src="../../js/jquery-1.12.4.min.js"></script>
<!-- Number Mask -->

<script async src="../../js/jquery.masknumber.js"></script>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>


    
</head>

<!-- Header -->
<?php include 'header.php'; ?>

  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Novo produto</h1>
    
    <form action="addProduct.php?acao=create" method="post" accept-charset="UTF-8">
      <div class="input-field">
        <label for="sku" class="label"><?php echo SKU; ?></label>
        <input type="text" id="sku" class="input-text" name="sku" required /> 
      </div>
      <div class="input-field">
          <label for="name" class="label"><?php echo PRODUTO_NOME; ?></label>
          <input type="text" id="name" class="input-text" name="nome" required /> 
      </div>
      <div class="input-field">
        <label for="price" class="label"><?php echo PRECO; ?></label>
        <input type="text" id="price" class="input-text" name="preco" data-decimal="," data-thousands="." required /> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label"><?php echo QUANTIDADE; ?></label>
        <input type="number" id="quantity" class="input-text" name="quantidade" required /> 
      </div>
      <div class="input-field">
        <label for="category" class="label"><?php echo CATEGORIAS; ?></label>
        <select multiple id="category" class="input-text" name="categorias[]" required>
          <?php foreach($listaCategorias as $key => $categoria){ ?>
          <option value="<?php echo strtoupper($categoria['nomeCateg']); ?>" <?php if($key==0){ echo "selected"; } ?>><?php echo strtoupper($categoria['nomeCateg']); ?></option>
          <?php } ?>          
        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label"><?php echo DESCRICAO; ?></label>
        <textarea id="description" class="input-text" name="descricao" required></textarea>
      </div>
      <div class="actions-form">
        <a href="products.php" class="action back"><?php echo VOLTAR; ?></a>
        <input class="btn-submit btn-action" type="submit" value="<?php echo SALVAR_PRODUTO; ?>" />
      </div>
      
    </form>
  </main>
  <!-- Main Content -->

<!-- Footer -->
<?php include 'footer.php'; ?>
<script type="text/javascript">
        $(document).ready(function () {
            $('[name=preco]').maskNumber();
        });
    </script>
</body>
</html>
