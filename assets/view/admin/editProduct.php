<!doctype html>
<?php 
    require_once '../../controllers/ProdutoController.php';
    require_once '../../controllers/CategoriaController.php';
    
    $db = new Mysql();
    $bdCategorias = new CategoriaController($db);
    $listaCategorias = $bdCategorias->acaoListarTodasCategorias();
    
    if(isset($_GET['id'])){        
    
    $produtos = new ProdutoController($db);
    
        if(isset($_GET['acao'])&&$_GET['acao']=='update'){
            $produtoEditar = $produtos->acaoEditarProduto($_GET['id'],$_POST['nome'],$_POST['sku'],$_POST['descricao'],$_POST['quantidade'],$_POST['preco'],$_POST['categoria']);
        }else{
            $produtoEditar = $produtos->acaoEncontrarProduto($_GET['id']);
        }          
           // if($editado){ echo "Editado com suceeso"; }
    }else{
        /* redirecionar para algum lugar */
    }
?>
<html>
<head>
  <title>Webjump | Backend Test | Add Product</title>
  <meta charset="utf-8">

<link  rel="stylesheet" type="text/css"  media="all" href="../../css/style.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
<meta name="viewport" content="width=device-width,minimum-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script></head>
 
<!-- Header -->
<?php include 'header.php'; ?>

  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Editar Produto</h1>
    <span style="color:green;font-weight:bold;"> <?php if(isset($_GET['sucesso']) && $_GET['sucesso']==1){ echo "Editado com sucesso!"; } ?></span>
    <form action="editProduct.php?acao=update&id=<?php echo $produtoEditar['id']; ?>" method="post" accept-charset="UTF-8">
      <div class="input-field">
        <label for="sku" class="label"><?php echo SKU; ?></label>
        <input type="text" id="sku" class="input-text" value="<?php echo $produtoEditar['sku']; ?>" name="sku" /> 
        <input type="hidden" name="id" value="<?php echo $produtoEditar['id']; ?>">
      </div>
      <div class="input-field">
        <label for="name" class="label"><?php echo PRODUTO_NOME; ?></label>
        <input type="text" id="name" class="input-text" value="<?php echo $produtoEditar['nome']; ?>" name="nome"  /> 
      </div>
      <div class="input-field">
        <label for="price" class="label"><?php echo PRECO; ?></label>
        <input type="text" id="price" class="input-text" value="<?php echo number_format($produtoEditar['preco'], 2, ',', '.'); ?>" name="preco"  /> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label"><?php echo QUANTIDADE; ?></label>
        <input type="text" id="quantity" class="input-text" value="<?php echo $produtoEditar['quantidade']; ?>" name="quantidade"  /> 
      </div>
      <div class="input-field">
        <label for="category" class="label"><?php echo CATEGORIAS; ?></label>
        <select multiple id="category" class="input-text" name="categoria[]">
        <?php
            $categorias = explode("|", $produtoEditar['categoria']);             
            foreach($listaCategorias as $listaBdCategoria){
                $flag = false;                
                foreach($categorias as $categoria){ 
                   //print_r($categoria);print_r("==");print_r($listaBdCategoria['nomeCateg']); print_r('<br>');
                    if(strtoupper($categoria)==strtoupper($listaBdCategoria['nomeCateg'])) { ?>
                    <option value="<?php echo $categoria; ?>" selected=""><?php echo $categoria; ?></option>
                <?php $flag = true; break; } }?>                     
                <?php if(!$flag){ ?>
                     <option value="<?php echo $listaBdCategoria['nomeCateg']; ?>" ><?php echo $listaBdCategoria['nomeCateg']; ?></option>
                <?php } ?>
            <?php } ?>            
        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label"><?php echo DESCRICAO; ?></label>
        <textarea id="description" class="input-text" name="descricao"><?php echo $produtoEditar['descricao']; ?> </textarea>
      </div>
      <div class="actions-form">
        <a href="products.php" class="action back"><?php echo VOLTAR; ?></a>
        <input class="btn-submit btn-action" type="submit" value="<?php echo EDITAR_PRODUTO; ?>" />
      </div>
      
    </form>
  </main>
  <!-- Main Content -->

<!-- Footer -->
<?php include 'footer.php'; ?> 
</body>
</html>
