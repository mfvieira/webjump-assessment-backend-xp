<!doctype html>
<?php 
    require_once '../../controllers/ProdutoController.php';
    require_once '../../controllers/CategoriaController.php';
    
    $db = new Mysql();
    
    $produtos = new ProdutoController($db);
    $listaProdutos = $produtos->acaoListarTodosProdutos();

?>
<html>
<head>
  <title>Webjump | Backend Test | Dashboard</title>
  <meta charset="utf-8">

<link  rel="stylesheet" type="text/css"  media="all" href="../../css/style.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
<meta name="viewport" content="width=device-width,minimum-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script></head>
  <!-- Header -->
  <?php include 'header.php'; ?>
  
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title"><?php echo DASHBOARD;?></h1>
    </div>
    <div class="infor">
      Sua loja tem o total de <?php echo $listaProdutos->rowCount(); ?> produtos. <a href="addProduct.php" class="btn-action"><?php echo ADD_NOVO_PRODUTO;?></a>
    </div>
    <ul class="product-list">
    <?php foreach($listaProdutos as $produto){ ?>    
      <li>
        <div class="product-image">
          <img src="../../images/product/tenis-runner-bolt.png" layout="responsive" width="164" height="145" alt="Tênis Runner Bolt" />
        </div>
        <div class="product-info">
          <div class="product-name"><span><?php echo $produto['nome']; ?></span></div>
          <div class="product-price"><span class="special-price"><?php echo $produto['quantidade']; ?> available</span> <span>R$<?php echo $produto['preco']; ?></span></div>
        </div>
      </li>
    <?php } ?>  
    </ul>
  </main>
  <!-- Main Content -->
<!--footer-->
<?php include 'footer.php'; ?>
  </body>
</html>
