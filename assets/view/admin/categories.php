<!doctype html>
<?php 
    require_once '../../controllers/CategoriaController.php';
    
    $db = new Mysql();
    $categorias = new CategoriaController($db);
    $listaCategorias = $categorias->acaoListarTodasCategorias();

    if(isset($_GET['acao'])){
        if($_GET['acao']=='delete'){   
            $deletado = $categorias->acaoDeletarCategoria($_GET['id']);            
    }}
    
    if(isset($_GET['acao'])){
        if($_GET['acao']=='edit'){   
            $editado = $categorias->acaoEditarCategoria($_GET['id']);
    }}
?>
<html>
<head>
  <title>Webjump | Backend Test | Categories</title>
  <meta charset="utf-8">

<link  rel="stylesheet" type="text/css"  media="all" href="../../css/style.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
<meta name="viewport" content="width=device-width,minimum-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script></head>
 
<!-- Header -->
<?php include 'header.php'; ?>
<body>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
        <span style="color:green;font-weight:bold;"> 
              <?php if(isset($_GET['sucesso']) && $_GET['sucesso']==1){ echo "Criado com sucesso!"; } ?>
              <?php if(isset($_GET['deletado']) && $_GET['deletado']==1){ echo "Deletado com sucesso!"; } ?>
          </span> 
      <h1 class="title"><?php echo CATEGORIAS; ?></h1>
      <a href="addCategory.php" class="btn-action"><?php echo ADICIONAR_CATEGORIA; ?></a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content"><?php echo CATEGORIA_NOME; ?></span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content"><?php echo CODIGO; ?></span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content"><?php echo ACOES; ?></span>
        </th>
      </tr>
      <?php foreach($listaCategorias as $categoria){ ?>
      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?php echo $categoria['nomeCateg']; ?></span>
        </td>
      
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?php echo $categoria['codigoCateg']; ?></span>
        </td>
      
        <td class="data-grid-td">
          <div class="actions">
            <a href="editCategory.php?id=<?php echo $categoria['idCateg']; ?>">
                <div class="action edit"><span><?php echo EDITAR; ?></span></div></a>
            <a href="categories.php?acao=delete&id=<?php echo $categoria['idCateg']; ?>">
                <div class="action delete"><span><?php echo DELETAR; ?></span></div></a>
          </div>
        </td>
      </tr>
      <?php } ?>     
    </table>
  </main>
  <!-- Main Content -->

<!-- Footer -->
<?php include 'footer.php'; ?>
</body>
</html>
