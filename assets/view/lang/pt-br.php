<?php

/* Arquivo de Internacionalização PT-BR */

/* Definição de Produtos */
define("SKU", "Produto SKU");
define("PRODUTO_NOME", "Produto Nome");
define("PRECO", "Preço R$");
define("QUANTIDADE", "Quantidade");
define("CATEGORIAS", "Categorias");
define("DESCRICAO", "Descrição");
define("ADD_NOVO_PRODUTO", "Adicionar Novo Produto");

/* Definição de Categorias */
define("CATEGORIA_NOME", "Categoria Nome");
define("CODIGO", "Código");


/* Botões & Links */
define("VOLTAR", "Voltar");
define("SALVAR_PRODUTO", "Salvar Produto");
define("ADICIONAR_PRODUTO", "Adicionar Produto");
define("ADICIONAR_CATEGORIA", "Adicionar Categoria");
define("EDITAR_PRODUTO","Editar Produto");
define("EDITAR", "Editar");
define("DELETAR","Deletar");

/* Tabelas & Outros */
define("ACOES","Ações");
define("DASHBOARD","Dashboard");
