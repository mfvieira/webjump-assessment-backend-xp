<?php

require_once '../../classes/Categoria.php';
require_once '../../classes/Mysql.php';


class CategoriaController
{     
    protected $database = null;    

    public function __construct(DB $database)
    {  
        $this->database = $database;
        $this->database->connect();
    }
    
    public function acaoListarTodasCategorias()
    {          
       // buscar todas $categoria do banco de dados        
       $categoria = new Categoria($this->database);
       return  $categoria->getCategorias();   
        
    }
    
    public function acaoEncontrarCategoria($id)
    {         
       // buscar uma categoria do banco de dados        
       $categoria = new Categoria($this->database);
       return  $categoria->findCategoria($id);   
        
    }
    
    public function acaoDeletarCategoria($id)
    {       
        // editar $categoria do banco de dados
        $categoria = new Categoria($this->database);
        return $categoria->deletarCategoria($id); 
    }
    
    public function acaoEditarCategoria($id,$nome)   
    {     
        // edita $categoria do banco de dados
        $categoria = new Categoria($this->database);
                
        if(!$categoria->validaCategoria($nome)){
           $categoria->editarCategoria($id,$nome); 
        }else{
           header("Location: {$_SERVER['HTTP_REFERER']}");
        }
    }
    
    public function acaoCriarCategoria($nome,$codigo)       {       
        
        // criar uma nova categoria no banco de dados
        $categoria = new Categoria($this->database);
        
        if(!$categoria->validaCategoria($nome)){
           $categoria->criarCategoria($nome,$codigo);  
        }else{
           header("Location:addCategory.php?sucesso=0");
        }
        
    }  
    
    
}


