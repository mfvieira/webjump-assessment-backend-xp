<?php

require_once '../../classes/Produto.php';
require_once '../../classes/Mysql.php';


class ProdutoController
{     
    protected $database = null;    

    public function __construct(DB $database)
    {  
        $this->database = $database;
        $this->database->connect();
    }
    
    public function acaoListarTodosProdutos()
    {          
       // buscar todos produtos do banco de dados        
       $produto = new Produto($this->database);
       return  $produto->getProdutos();          
    }
    
    public function acaoEncontrarProduto($id)
    {         
       // buscar todos produtos do banco de dados        
       $produto = new Produto($this->database);
       return  $produto->findProduto($id);           
    }    
    
    public function acaoDeletarProduto($id)
    {       
        // deletar produto do banco de dados
        $produto = new Produto($this->database);
        return $produto->deletarProduto($id); 
    }
    
    /* Valida e/ou Formata & Encaminha para Produtos os dados */
    public function acaoEditarProduto($id,$nome,$sku,$descricao,$quantidade,$preco,$categorias)    {                    
        
        $preco = $this->formatarMoeda($preco);           
        $categorias = $this->formatarCategorias($categorias);
        
        // edita produto do banco de dados
        $produto = new Produto($this->database);
        return $produto->editarProduto($id,$nome,$sku,$descricao,$quantidade,$preco,$categorias); 
    }
    
    /* Valida e/ou Formata & Encaminha para Produtos os dados */
    public function acaoCriarProduto($nome,$sku,$descricao,$quantidade,$preco,$categorias)    {                    
       
        $preco = $this->formatarMoeda($preco);   
        $categorias = $this->formatarCategorias($categorias);
        
        // criar um novo produto do banco de dados
        $produto = new Produto($this->database);
        return $produto->criarProduto($nome,$sku,$descricao,$quantidade,$preco,$categorias); 
    }
    
    /* Formata Moeda para salvar no BD */
    public function formatarMoeda($valor){
        
        $valor = str_replace(".", "", $valor); // retira o Mil "." em portugues
        $valor = str_replace(",", ".", $valor); // troca para versao aceita no banco de dados  
        
        return $valor;
    }
    
    /* Formata da Lista de categorias para ser usada no campo categorias de produtos */
    public function formatarCategorias($categorias){
       
        $categoriaFormatada = "";
        foreach($categorias as $categoria){            
            $categoriaFormatada .= $categoria."|";            
        }        
        return rtrim($categoriaFormatada ,"|");
    }    
    
}


