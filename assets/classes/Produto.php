<?php

class Produto
{
    private $database = null;

    public function __construct(DB $database)
    { 
        $this->database = $database; 
    }

    /* Retorna lista de todos os produtos */
    public function getProdutos()
    {
        $produtos = $this->database->query('SELECT * FROM produtos ORDER BY id DESC');
        return $produtos;
    }  
    
    /* Deleta um produto */
    public function deletarProduto($id)
    { 
        $produto = $this->database->query('Delete FROM produtos WHERE id ='.$id); 
        header("Location: {$_SERVER['HTTP_REFERER']}?deletado=".$produto->rowCount());     
    } 
    
    /* Busca um produto solicitado e o retorna se existir */
    public function findProduto($id)
    { 
        $produto = $this->database->query('SELECT * FROM produtos WHERE id ='.$id); 
        return $produto->fetch(PDO::FETCH_ORI_FIRST);
    } 
    
    /* Edita um produto */
    public function editarProduto($id,$nome,$sku,$descricao,$quantidade,$preco,$categoria)
    { 
        $produto = $this->database->query('UPDATE produtos SET nome="'.$nome.'" '
                . ', sku="'.$sku.'" , descricao="'.$descricao.'"  '
                . ', quantidade='.$quantidade.' , preco='.$preco.' '
                . ', categoria="'.$categoria.'" WHERE id ='.$id); 
        
        header("Location: {$_SERVER['HTTP_REFERER']}&sucesso=".$produto->rowCount());
    } 
    
    /* Cria um novo produto */
    public function criarProduto($nome,$sku,$descricao,$quantidade,$preco,$categoria)
    { 
        $produto = $this->database->query('INSERT INTO produtos (nome,sku,descricao,quantidade,preco,categoria) VALUES ("'.$nome.'" , "'.$sku.'" ,"'.$descricao.'" ,'.$quantidade.','.$preco.',"'.$categoria.'" )'); 
        header("Location:products.php?sucesso=".$produto->rowCount());     
    } 
}
 
 
?>