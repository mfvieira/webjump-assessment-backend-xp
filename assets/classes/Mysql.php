<?php

require_once 'DB.php';

class Mysql implements DB
{
    protected $db = null;
    
    protected $dsn = 'mysql:host=localhost;dbname=webjump';
    protected $user = 'root';
    protected $pass = '';     
        
    
    /* Função para connectar com o Banco Mysql */
    public function connect()
    {   
        /* Arquivo de Log Mysql */
        $file = 'mysql_log.txt';
        
        try{
            $this->db = new PDO($this->dsn, $this->user, $this->pass); 
        
        /* Log erro de accesso negado */
        } catch (PDOException $ex){
             echo 'Acesso negado ao conectar com o bando de dados.';
             $error = '['.date('Y-m-d').'] '.$ex->getMessage()."\n";
             file_put_contents($file, $error, FILE_APPEND | LOCK_EX);
             
        } 
        
        /* Log de erro */
        catch (Exception $ex) {
             echo 'Não é possível conectar ao servidor de bando de dados';
             $error = '['.date('Y-m-d').'] '.$ex->getMessage()."\n";
             file_put_contents($file, $error, FILE_APPEND | LOCK_EX);
        }   
    }

    /* Função usada para realizar as queries */
    public function query($query)
    {
        /* Arquivo de Log Mysql */
        $file = 'mysql_log.txt';
       
        try{
            $stm = $this->db->query($query);  // print_r($query);  
        } catch (Exception $ex) {
            echo 'Erro na Query: ';
             $error = '['.date('Y-m-d').'] '.$ex->getMessage()."\n";
             file_put_contents($file, $error, FILE_APPEND | LOCK_EX);
        }
        return $stm;
    }
}

?>

