<?php

class Categoria
{
    private $database = null;

    public function __construct(DB $database)
    { 
        $this->database = $database; 
    }
    
    /* Retorna Lista de Categorias */
    public function getCategorias()
    {
        $categoria = $this->database->query('SELECT * FROM categorias ORDER BY idCateg DESC');
        return $categoria;
    }  
    
    /* Deleta Categoria */
    public function deletarCategoria($id)
    { 
        $categoria = $this->database->query('Delete FROM categorias WHERE idCateg ='.$id); 
        header("Location:categories.php?deletado=".$categoria->rowCount());
    } 
    
    /* Busca um Categoria */
    public function findCategoria($id)
    { 
        $categoria = $this->database->query('SELECT * FROM categorias WHERE idCateg ='.$id); 
        return $categoria->fetch(PDO::FETCH_ORI_FIRST);
    } 
    
    /* Edita uma Categoria - Categorias salvas em Letras Maiusculas */
    public function editarCategoria($id,$nome)
    { 
        // All Caps
        $nome = strtoupper($nome);
        
        $categoria = $this->database->query('UPDATE categorias SET nomeCateg="'.$nome.'" WHERE idCateg ='.$id);         
        header("Location: {$_SERVER['HTTP_REFERER']}&sucesso=".$categoria->rowCount());
    } 
    
    /* Cria uma nova Categoria - Todas em Letras Maiusculas */
    public function criarCategoria($nome,$codigo)
    { 
        // All Caps
        $nome = strtoupper($nome);
        
        $categoria = $this->database->query('INSERT INTO categorias (nomeCateg,codigoCateg) VALUES ("'.$nome.'" ,'.$codigo.')'); 
        header("Location:categories.php?sucesso=".$categoria->rowCount());
     
    } 
    
    /* Verifica se existe um outra categoria com o mesmo nome */
    public function validaCategoria($nome)
    { 
        $categoria = $this->database->query('SELECT nomeCateg FROM categorias WHERE nomeCateg = "'.$nome.'"'); 
        return $categoria->rowCount();     
    } 
}

?>